#include "../header/window.h"

Window::Window(const std::string & l_windowTitle, const sf::Vector2u & l_resolution, const int & l_framerate)
{
    this->m_isDone = false;
    this->m_isFullscreen = false;
    
    this->Setup(l_windowTitle, l_resolution, l_framerate);
}

Window::~Window()
{
    this->DestroyWindow();
}

void Window::Setup(const std::string & l_windowTitle, const sf::Vector2u & l_resolution, const int & l_framerate)
{

    this->m_framerate = l_framerate;
    this->m_windowResolution = l_resolution;
    this->m_windowTitle = l_windowTitle;    

    this->CreateWindow();

}

void Window::CreateWindow()
{
    auto style = (this->m_isFullscreen ? sf::Style::Fullscreen : sf::Style::Default);
    this->m_window.create({this->m_windowResolution.x, this->m_windowResolution.y}, this->m_windowTitle, style);
    this->m_window.setFramerateLimit(this->m_framerate);
}

void Window::DestroyWindow()
{
    this->m_window.close();
}

void Window::Update()
{
    sf::Event event;

    while(this->m_window.pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
        {
            this->m_isDone = true;
        }
        else if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::F5)
        {
            this->TootleFullscreen();
        }
    }
}

void Window::TootleFullscreen()
{
    this->m_isFullscreen = !this->m_isFullscreen;

    this->DestroyWindow();
    this->CreateWindow();
}

void Window::BeginDraw() 
{
    this->m_window.clear(sf::Color(204, 255, 255, 255));
}

void Window::EndDraw()
{
    this->m_window.display();
}

void Window::Draw(sf::Drawable& l_drawable)
{
    this->m_window.draw(l_drawable);
}
