#include "../header/rain.h"

#include <iostream>
Rain::Rain(const int & l_qtdRainDrop, const sf::Vector2u l_windowSize)
{
    
    this->m_qtdRainDrop = l_qtdRainDrop;
    this->m_windowSize = l_windowSize;

    for (int i=0; i<l_qtdRainDrop; ++i)
    {
        this->m_rainDrops.push_back(this->CreateRainDrop());
    }
}

sf::Color Rain::CreateColor()
{
    int red = rand() % 256;
    int blue = rand() % 256;
    int green = rand() % 256;

    return sf::Color(red, green, blue);
}

sf::RectangleShape Rain::CreateRainDrop() {

        float largura = 1 + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX/(2)));
        int y_position = (rand() % this->m_windowSize.y) - this->m_windowSize.y;

        sf::RectangleShape rain_drop(sf::Vector2f(largura, largura * 6));
        rain_drop.setFillColor(this->CreateColor());
        rain_drop.setPosition(rand() % this->m_windowSize.x, y_position);

        return rain_drop;
}

void Rain::UpdateRainDrop()
{
    for (int i=0; i<this->m_qtdRainDrop; ++i)
    {
        int vel = this->m_rainDrops[i].getSize().y * 2;
        this->m_rainDrops[i].setPosition(this->m_rainDrops[i].getPosition().x, this->m_rainDrops[i].getPosition().y+vel);

        if (this->m_rainDrops[i].getPosition().y > this->m_windowSize.y)
        {
            this->m_rainDrops.erase(this->m_rainDrops.begin() + i);
            this->m_rainDrops.push_back(this->CreateRainDrop());
        }
    }
}