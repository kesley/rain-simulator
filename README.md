# Rain simulation
A bidimensional rain simulation with depth perception.

## Dependencies
- SFML Devel
- g++

## 
```sh
# build source code
sh build.sh

# Run
./simulation
```
