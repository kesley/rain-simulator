#include <SFML/Graphics.hpp>
#include <string>

#ifndef WINDOW_H
#define WINDOW_H

class Window
{
    private:

        int m_framerate;
        
        bool m_isDone;
        bool m_isFullscreen;

        std::string m_windowTitle;
        sf::Vector2u m_windowResolution;
        sf::RenderWindow m_window;
        
        void CreateWindow();
        void DestroyWindow();

        void Setup(const std::string & l_windowTitle, const sf::Vector2u & l_resolution, const int & l_framerate);

    public:
        Window(const std::string & l_windowTitle, const sf::Vector2u & l_resolution, const int & l_framerate);
        ~Window();

        // Getter 
        bool IsDone() const { return this->m_isDone; }
        sf::Vector2u GetSize() const { return this->m_window.getSize(); }

        // Methods
        void TootleFullscreen();
        void Update();
        
        // Draw
        void BeginDraw();
        void Draw(sf::Drawable & l_drawnable);
        void EndDraw();
};


#endif