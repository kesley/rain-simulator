#include <iostream>

#include "../header/window.h"
#include "../header/rain.h"

int main()
{

    srand(time(NULL));

    Window window("Let it Rain", sf::Vector2u(1280, 720), 30);
    Rain rain(1000, window.GetSize());

    while (!window.IsDone()) {

        window.Update();
        rain.UpdateRainDrop();

        window.BeginDraw();

        for (int i=0; i<rain.GetQtdRainDrop(); ++i)
        {
            window.Draw(rain.GetRainDrop(i));
        }

        window.EndDraw();
        
    }

    return 0;
}